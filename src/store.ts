import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        status: '',
        accessToken:  localStorage.getItem('access_token') ||  '',
        currentUser : {},
    },
    mutations: {
        auth_request(state) {
            state.status = 'loading';
        },
        auth_error(state) {
            state.status = 'error';
        },
        logout(state) {
            state.status = '';
            state.accessToken = '';
        },
    },
    actions: {
        login({commit}, user) {
            return new Promise((resolve, reject) => {
                commit('auth_request');
                axios({url: 'http://localhost:8080/login', data: user, method: 'POST' })
                    .then((resp) => {
                        const token = resp.data.token;
                        const user = resp.data.user;
                        localStorage.setItem('token', token);
                        axios.defaults.headers.common.Authorization = token;
                        commit('auth_success', token, user);
                        resolve(resp);
                    })
                    .catch((err) => {
                        commit('auth_error');
                        localStorage.removeItem('token');
                        reject(err);
                    });
            });
        },
        register({commit}, user) {
            return new Promise((resolve, reject) => {
                commit('auth_request');
                axios({url: 'http://localhost:8080/register', data: user, method: 'POST' })
                    .then((resp) => {
                        const token = resp.data.token;
                        const user = resp.data.user;
                        localStorage.setItem('token', token);
                        axios.defaults.headers.common.Authorization = token;
                        commit('auth_success', token, user);
                        resolve(resp);
                    })
                    .catch((err) => {
                        commit('auth_error', err);
                        localStorage.removeItem('token');
                        reject(err);
                    });
            });
        },
        logout({commit}) {
            return new Promise((resolve, reject) => {
                commit('logout');
                localStorage.removeItem('token');
                delete axios.defaults.headers.common.Authorization;
                resolve();
            });
        },
    },
    getters : {
        isLoggedIn: (state) => !!state.accessToken,
        authStatus: (state) => state.status,
    },
});
