import Vue from 'vue';
import Buefy from 'buefy';
import 'buefy/dist/buefy.css';
import App from '@/App.vue';
import router from '@/router';
import store from '@/store';
import Axios from 'axios';
import './registerServiceWorker';
import '@mdi/font/css/materialdesignicons.min.css';

Vue.config.productionTip = false;
Vue.prototype.$http = Axios;
const accessToken = localStorage.getItem('access_token');

if (accessToken) {
  Vue.prototype.$http.defaults.headers.common.Authorization =  accessToken;
}

Vue.use(Buefy);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
