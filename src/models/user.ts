class User {
    name: string;
    username: string;
    email: string;
    password: string;

    constructor(inName: string, inUsername: string, inEmail: string, inPassword: string) {
        this.name = inName;
        this.username = inUsername;
        this.email = inEmail;
        this.password = inPassword;
    }
}
